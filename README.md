# ard_poster

Collection of the group's recent work on transfer learning for Amazon Research Days (4-5 Oct 2018).

* Alex's conditional risk
* Amelie's finetuning
* Asya's MTL with labeled and unlabeled tasks
* Mary's multi-output distillation
* Sylvestre's iCaRL
